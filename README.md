## Problem

Help HR manager Personia get a grasp of her ever-changing company’s hierarchy! Every week
Personia receives a JSON of employees and their supervisors from her demanding CEO Chris,
who keeps changing his mind about how to structure his company. Personia wants a tool to help
her better understand the employee hierarchy and respond to employee’s queries about who
their boss is.

## Solution

- Traverse tree dfs for validation and transformation
- Use tree (material path) to save hierarchy

## Features

- [x] API to post employee hierarchy
- [x] API to get full hierarchy tree
- [x] API to get supervisors

## Implementations

- [x] Postgres Database ([typeorm]).
- [x] Material Paths Tree
- [x] Config Service
- [x] Authen by Jwt Token-based
- [x] Docs: Swagger
- [x] Units tests
- [x] Docker

## Quick run

```bash
sudo ifconfig lo0 alias 10.10.10.10
cd HrmAssignment/
cp env-example .env
docker-compose build
docker-compose up
```

For check status run

```bash
docker-compose logs
```

## API Documentation

- Swagger: http://localhost:3000/docs

## Database utils

Generate migration

```bash
yarn migration:generate -- src/database/migrations/CreateNameTable
```

Run migration

```bash
yarn migration:run
```

Revert migration

```bash
yarn migration:revert
```

## Unit Tests

```bash
docker exec -it hrm /bin/bash
yarn test
```

## Demo Account

Please check:

SEED_USER_EMAIL=
SEED_USER_PASS=

in env-example file
