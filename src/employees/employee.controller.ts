import {
  Controller,
  Post,
  Body,
  UseGuards,
  HttpStatus,
  HttpCode,
  Get,
  Query,
} from "@nestjs/common";
import { EmployeesService } from "./employee.service";
import { ApiBearerAuth, ApiBody, ApiTags } from "@nestjs/swagger";
import { AuthGuard } from "@nestjs/passport";

@ApiBearerAuth()
@UseGuards(AuthGuard("jwt"))
@ApiTags("Employees")
@Controller({
  path: "employees",
  version: "1",
})
export class EmployeesController {
  constructor(private readonly employeesService: EmployeesService) {}

  @Post("/")
  @ApiBody({ description: "JSON hierarchy" })
  @HttpCode(HttpStatus.CREATED)
  create(@Body() dto: any) {
    return this.employeesService.create(dto);
  }

  @Get("/tree")
  @HttpCode(HttpStatus.OK)
  getFullTree() {
    return this.employeesService.getFullEmployeeTree();
  }

  @Get("/supervisors")
  @HttpCode(HttpStatus.OK)
  getSupervisors(@Query("name") name: string) {
    return this.employeesService.getSupervisors(name);
  }
}
