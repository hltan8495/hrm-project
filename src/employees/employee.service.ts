import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from "@nestjs/common";
import { InjectDataSource } from "@nestjs/typeorm";
import { isArray, isObject, isString } from "lodash";
import { DataSource } from "typeorm";
import { Employee } from "./entities/employee.entity";
import {
  EmployeeHierarchyJson,
  EmployeeHierarchyDbList,
  EmployeeHierarchyDbTree,
} from "./hierarchy";
import { JsonObject } from "./hierarchy/types";

@Injectable()
export class EmployeesService {
  constructor(
    @InjectDataSource()
    private dataSource: DataSource
  ) {}

  /**
   * Validation and save json hierarchy of employee
   * @param jsonObject
   * @returns Object
   */
  async create(jsonObject: any): Promise<JsonObject> {
    this.validateTreeInput(jsonObject);

    const employeeHierarchy = new EmployeeHierarchyJson()
      .setInput(jsonObject)
      .build();

    const values = employeeHierarchy.extractPairValues();

    await this.dataSource.manager.transaction(async (manager) => {
      manager.query("DELETE FROM employee");
      manager.query(
        `INSERT INTO employee (id, name, mpath, "parentId") VALUES 
        ${values
          .map((v) => `(${v.id}, '${v.name}','${v.mpath}', ${v.parentId})`)
          .join(",")}`
      );
    });

    return employeeHierarchy.getFullTree();
  }

  /**
   * Get full hierarchy of employee
   * @returns Object
   */
  async getFullEmployeeTree(): Promise<JsonObject> {
    const employees = await this.dataSource.manager
      .getTreeRepository(Employee)
      .findTrees();

    return new EmployeeHierarchyDbList()
      .setInput(employees)
      .build()
      .getFullTree();
  }

  /**
   * Find supervisor and supervisor's supervisor
   * @param name Name of an employee
   */
  async getSupervisors(name: string): Promise<JsonObject> {
    const employee = await this.dataSource.manager
      .getRepository(Employee)
      .findOne({ where: { name } });

    if (!employee) {
      throw new NotFoundException("Employee not found");
    }

    const supervisorData = await this.dataSource.manager
      .getTreeRepository(Employee)
      .findAncestorsTree(employee, { depth: 3 });

    if (!supervisorData.parent) {
      throw new BadRequestException(`${name} is boss. No supervisor found`);
    }

    return new EmployeeHierarchyDbTree()
      .setInput(supervisorData.parent)
      .setLevel(2)
      .build()
      .getFullTree();
  }

  /**
   * Validate json input
   * @param jsonObject
   */
  private validateTreeInput(jsonObject: JsonObject): void {
    if (isArray(jsonObject) || !isObject(jsonObject)) {
      throw new BadRequestException("Input is not object");
    }

    const keys = Object.keys(jsonObject);
    keys.forEach((key) => {
      if (key === jsonObject[key]) {
        throw new BadRequestException(`Incorrect supervision at ${key}`);
      }
      if (!isString(jsonObject[key])) {
        throw new BadRequestException(`Input value is not string at ${key}`);
      }
    });
  }
}
