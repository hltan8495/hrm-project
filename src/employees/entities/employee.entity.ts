import {
  Entity,
  Column,
  Tree,
  TreeChildren,
  TreeParent,
  PrimaryColumn,
} from "typeorm";

@Entity()
@Tree("materialized-path")
export class Employee {
  @PrimaryColumn()
  id: number;

  @Column()
  name: string;

  @TreeChildren()
  children?: Employee[];

  @TreeParent()
  parent?: Employee;
}
