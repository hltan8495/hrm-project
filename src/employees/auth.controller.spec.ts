import { ConfigModule } from "@nestjs/config";
import { Test, TestingModule } from "@nestjs/testing";
import { registerAs } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { TypeOrmConfigService } from "../database/typeorm-config.service";
import { DataSource } from "typeorm";
import { EmployeesController } from "./employee.controller";
import { EmployeesService } from "./employee.service";

describe("EmployeesController", () => {
  let employeeController: EmployeesController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          load: [
            registerAs("database", () => ({
              type: process.env.DATABASE_TYPE,
              host: process.env.DATABASE_HOST,
              port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
              password: process.env.DATABASE_PASSWORD,
              name: process.env.DATABASE_TEST_NAME,
              username: process.env.DATABASE_USERNAME,
              synchronize: true,
              logging: false,
            })),
          ],
          envFilePath: [".env"],
        }),
        TypeOrmModule.forRootAsync({
          useClass: TypeOrmConfigService,
          dataSourceFactory: async (options) => {
            return await new DataSource(options).initialize();
          },
        }),
      ],
      providers: [EmployeesService],
      controllers: [EmployeesController],
    }).compile();

    employeeController = app.get<EmployeesController>(EmployeesController);
  });

  describe("Function: Create Employee Hierarchy", () => {
    it("TC01: Input Is Not Object", async () => {
      try {
        await employeeController.create(["Nick", "Pete"]);
      } catch (e) {
        expect(e.message).toBe("Input is not object");
      }
    });

    it("TC02: Value Is Not String", async () => {
      try {
        await employeeController.create({
          Pete: 1,
          Nick: "Sophie",
          Sophie: "Jonas",
        });
        expect(0).toBe(1);
      } catch (e) {
        expect(e.message).toBe("Input value is not string at Pete");
      }
    });

    it("TC03: Incorrect Self Supervision", async () => {
      try {
        await employeeController.create({
          Nick: "Nick",
        });
        expect(0).toBe(1);
      } catch (e) {
        expect(e.message).toBe("Incorrect supervision at Nick");
      }
    });

    it("TC04: Successfully Created", async () => {
      const data = await employeeController.create({
        Pete: "Nick",
        Barbara: "Nick",
        Nick: "Sophie",
        Sophie: "Jonas",
      });
      expect(data).toEqual({
        Jonas: { Sophie: { Nick: { Pete: {}, Barbara: {} } } },
      });
    });

    it("TC05: Loop Tree", async () => {
      try {
        await employeeController.create({
          Pete: "Nick",
          Barbara: "Nick",
          Nick: "Sophie",
          Sophie: "Pete",
        });
        expect(0).toBe(1);
      } catch (e) {
        expect(e.message).toBe("Loop Found");
      }
    });

    it("TC06: Multiple tree input", async () => {
      try {
        await employeeController.create({
          Pete: "Nick",
          Barbara: "Nick",
          Sophie: "Jonas",
        });
        expect(0).toBe(1);
      } catch (e) {
        expect(e.message).toBe("Multiple tree input");
      }
    });
  });

  describe("Function: Get Full Employee Hierarchy", () => {
    it("TC01: Successfully Get Full Tree #1", async () => {
      await employeeController.create({
        Pete: "Nick",
        Barbara: "Nick",
        Nick: "Sophie",
        Sophie: "Jonas",
      });
      const tree = await employeeController.getFullTree();
      expect(tree).toEqual({
        Jonas: { Sophie: { Nick: { Pete: {}, Barbara: {} } } },
      });
    });

    it("TC02: Successfully Get Full Tree #2", async () => {
      await employeeController.create({
        A: "B",
        B: "C",
        H: "C",
      });
      const tree = await employeeController.getFullTree();
      expect(tree).toEqual({
        C: {
          B: {
            A: {},
          },
          H: {},
        },
      });
    });

    it("TC03: Successfully Get Full Tree #3", async () => {
      await employeeController.create({
        Z: "A",
        A: "B",
        B: "C",
        C: "D",
        D: "E",
        E: "F",
        F: "G",
        G: "H",
        K: "H",
      });
      const tree = await employeeController.getFullTree();
      expect(tree).toEqual({
        H: {
          G: {
            F: {
              E: {
                D: {
                  C: {
                    B: {
                      A: {
                        Z: {},
                      },
                    },
                  },
                },
              },
            },
          },
          K: {},
        },
      });
    });
  });

  describe("Function: Get Supervisors", () => {
    it("TC01: Successfully Get Supervisors", async () => {
      await employeeController.create({
        Z: "A",
        A: "B",
        B: "C",
        C: "D",
        D: "E",
        E: "F",
        F: "G",
        G: "H",
      });
      const response = await employeeController.getSupervisors("E");
      expect(response).toEqual({
        G: {
          F: {},
        },
      });
    });

    it("TC02: Find Boss's Supervisors", async () => {
      try {
        await employeeController.create({
          A: "B",
          B: "C",
          C: "D",
          D: "E",
        });
        await employeeController.getSupervisors("E");
        expect(0).toBe(1);
      } catch (e) {
        expect(e.message).toBe("E is boss. No supervisor found");
      }
    });
  });
});
