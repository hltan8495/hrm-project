export interface MaterialItem {
  id: number;
  name: string;
  mpath: string;
  parentId?: number;
}

export type JsonObject = { [key: string]: any };
