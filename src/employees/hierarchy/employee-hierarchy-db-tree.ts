import { NotImplementedException } from "@nestjs/common";
import { Employee } from "../entities/employee.entity";
import { HierarchyBase } from "./hierarchy-base";
import { MaterialItem } from "./types";

export class EmployeeHierarchyDbTree extends HierarchyBase<Employee> {
  build(): EmployeeHierarchyDbTree {
    this.fullTree = this.traverse(this.input, this.level);
    return this;
  }

  private traverse(
    employee: Employee,
    maxLevel: number
  ): { [key: string]: any } {
    let object = {};
    let cur = employee;
    let level = 1;
    while (cur && level++ <= maxLevel) {
      object = {
        [cur.name]: { ...object },
      };
      cur = cur.parent;
    }
    return object;
  }

  extractPairValues(): MaterialItem[] {
    throw new NotImplementedException(
      "EmployeeHierarchyDbTree.extractPairValues"
    );
  }
}
