import { BadRequestException } from "@nestjs/common";
import { union } from "lodash";
import { HierarchyBase } from "./hierarchy-base";
import { MaterialItem } from "./types";

export class EmployeeHierarchyJson extends HierarchyBase<{
  [key: string]: string;
}> {
  build() {
    let tempTree = {};

    const boss: any = Object.values(this.input).find(
      (value: any) => !(value in this.input)
    );

    if (!boss) {
      throw new BadRequestException("Loop Found");
    }

    const entries = Object.entries(this.input).flat();
    const keys = Object.keys(this.input);
    const values = Object.values(this.input);
    const all = union(keys, values);

    entries.forEach((entry: any) => (tempTree[entry] = { [entry]: [] }));

    keys.forEach((key) => {
      tempTree[this.input[key]][this.input[key]].push(tempTree[key]);
    });

    if (!tempTree[boss]) {
      throw new BadRequestException("Invalid JSON Input");
    }

    const nodeCount = { total: 0 };
    this.fullTree = this.traverse(tempTree[boss], {}, nodeCount);

    if (all.length !== nodeCount.total) {
      throw new BadRequestException("Multiple tree input");
    }

    return this;
  }

  private traverse(tree: any, object = {}, nodeCount: { total: number }) {
    const keys = Object.keys(tree);

    if (!keys.length) {
      return;
    }

    keys.forEach((key) => {
      if (!object[key]) {
        object[key] = {};
        nodeCount.total++;
      }

      if (!tree[key].length) return;

      tree[key].forEach((node: any) => {
        this.traverse(node, object[key], nodeCount);
      });
    });

    return object;
  }

  extractPairValues(): MaterialItem[] {
    const employees = [
      ...new Set([...Object.values(this.input), ...Object.keys(this.input)]),
    ];

    const employeesByName = employees.reduce(
      (accumulator: any, current: string, index) => {
        accumulator[current] = index;
        return accumulator;
      },
      {}
    );

    return this.buildMaterialPaths(this.fullTree, employeesByName);
  }

  private buildMaterialPaths(
    tree = {},
    employees: any,
    paths = [],
    mpath = "",
    supervisor?: string
  ) {
    const keys = Object.keys(tree);

    if (!keys.length) {
      return;
    }

    keys.forEach((key) => {
      const employeeId = employees[key];
      const supervisorId = employees[supervisor];

      mpath = mpath.concat(employeeId + ".");

      paths.push({
        id: employeeId,
        name: key,
        mpath,
        parentId: supervisor ? supervisorId : null,
      });

      if (!Object.keys(tree[key]).length) return;

      this.buildMaterialPaths(tree[key], employees, paths, mpath, key);
    });

    return paths;
  }
}
