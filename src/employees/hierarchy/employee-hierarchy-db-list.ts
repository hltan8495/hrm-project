import { NotImplementedException } from "@nestjs/common";
import { Employee } from "../entities/employee.entity";
import { HierarchyBase } from "./hierarchy-base";
import { JsonObject, MaterialItem } from "./types";

export class EmployeeHierarchyDbList extends HierarchyBase<Employee[]> {
  build(): EmployeeHierarchyDbList {
    this.fullTree = this.traverse(this.input);
    return this;
  }

  private traverse(employees: Employee[], object = {}): JsonObject {
    if (!employees || !employees.length) {
      return {};
    }

    employees.forEach(({ name, children }) => {
      if (!object[name]) {
        object[name] = {};
      }

      if (!children.length) return {};

      this.traverse(children, object[name]);
    });

    return object;
  }

  extractPairValues(): MaterialItem[] {
    throw new NotImplementedException(
      "EmployeeHierarchyDbList.extractPairValues"
    );
  }
}
