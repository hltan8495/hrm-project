import { JsonObject, MaterialItem } from "./types";

export abstract class HierarchyBase<T> {
  protected input: T;
  protected fullTree: JsonObject;
  protected level: number;

  setInput(input: any): HierarchyBase<T> {
    this.input = input;
    return this;
  }

  setLevel(level: number): HierarchyBase<T> {
    this.level = level;
    return this;
  }

  getFullTree(): JsonObject {
    return this.fullTree;
  }

  abstract build(): HierarchyBase<T>;

  abstract extractPairValues(): MaterialItem[];
}
