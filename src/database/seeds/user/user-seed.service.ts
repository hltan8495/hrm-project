import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "../../../users/entities/user.entity";
import { Repository } from "typeorm";

@Injectable()
export class UserSeedService {
  constructor(
    @InjectRepository(User)
    private repository: Repository<User>
  ) {}

  async run() {
    const email = process.env.SEED_USER_EMAIL;
    const countUser = await this.repository.count({
      where: {
        email,
      },
    });

    if (countUser === 0) {
      await this.repository.save(
        this.repository.create({
          email,
          password: process.env.SEED_USER_PASS,
        })
      );
    }
  }
}
