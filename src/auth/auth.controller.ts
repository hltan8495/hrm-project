import { Body, Controller, HttpCode, HttpStatus, Post } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { ApiTags } from "@nestjs/swagger";
import { AuthLoginDto } from "./dto/auth-login.dto";

@ApiTags("Auth")
@Controller({
  path: "auth",
  version: "1",
})
export class AuthController {
  constructor(public service: AuthService) {}

  @Post("email/login")
  @HttpCode(HttpStatus.OK)
  public async login(@Body() loginDto: AuthLoginDto) {
    return this.service.validateLogin(loginDto);
  }
}
