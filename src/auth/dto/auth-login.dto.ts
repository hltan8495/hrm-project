import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsString } from "class-validator";
import { Transform } from "class-transformer";

export class AuthLoginDto {
  @ApiProperty({ example: "test1@example.com" })
  @Transform(({ value }) => value.toLowerCase().trim())
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsString()
  password: string;
}
